# Method 1

probOne := method("Hello, World.")
 
 probOne println
 # method 2


myString := ("Book")
mySubString := ("ook")
probTwo := method(myString, mySubString)
	myString == mySubString 
	myString findSeq(mySubString)

# method 3

myList := list( 62,	12,	8,	60,	94,
				18,	70,	60,	44,	85,
				55,	21,	48,	83,	5,
				31,	59,	82,	9,	6,
				26,	2,	46,	98,	11,
				43,	76,	76,	28,	91,
				62,	30,	38,	46,	12,
				65,	65,	59,	10,	17,
				79,	36,	94,	86,	40,
				95,	92,	48,	5,	48,
				83,	7,	74,	98,	78,
				59,	21,	58,	18,	16,
				72,	1,	11,	13,	58,
				52,	34,	77,	22,	18,
				99,	8,	89,	69,	60,
				5,	23,	45,	70,	51,
				6,	2,	21,	26,	62,
				76,	4,	50,	86,	92,
				58,	57,	48,	82,	86,
				29,	12,	96,	37,	34)

myList foreach(i, i println)

myOtherList := myList sort()

myOtherList reverse(sort)

myOtherList select(isOdd)

# method 4

myList4 := list( 62, 12, 8,	60,	94,
				18,	70,	60,	44,	85,
				55,	21,	48,	83,	5,
				31,	59,	82,	9,	6,
				26,	2,	46,	98,	11,
				43,	76,	76,	28,	91,
				62,	30,	38,	46,	12,
				65,	65,	59,	10,	17,
				79,	36,	94,	86,	40,
				95,	92,	48,	5,	48,
				83,	7,	74,	98,	78,
				59,	21,	58,	18,	16,
				72,	1,	11,	13,	58,
				52,	34,	77,	22,	18,
				99,	8,	89,	69,	60,
				5,	23,	45,	70,	51,
				6,	2,	21,	26,	62,
				76,	4,	50,	86,	92,
				58,	57,	48,	82,	86,
				29,	12,	96,	37,	34)

List do (
    sortMe := method(
        size repeat(i,
            swapIndices(i, indexOf(slice(i, size) min))
        )
    )
)
 

myList4 sortMe println

# method 5

 problemFive := method(fib,
    if(fib <= 1, fib, fib_recur(fib - 1) + fib_recur(fib - 2) )
)

problemFive(10) println

# method 6

problemSix := method(fib,
    spaghetti := 0
    withA := 1
    drill := 0
    for(i, fib, 1, -1,
        drill = spaghetti + withA
        withA = spaghetti
        spaghetti = drill
        )

    
     spaghetti
)


# method seven




